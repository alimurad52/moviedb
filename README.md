## The Movie DB
Retrieving data from [https://www.themoviedb.org/](https://www.themoviedb.org/) to show the Popular and Trending movies along with Movie and Actor details with React JS.

## Available Scripts
### `npm install`
Run `npm install` to install all dependicies
### `npm start`
You can run the project and then head to your [http://localhost:3000](http://localhost:3000) to see the result.

### Heroku URL
`http://infinite-headland-23865.herokuapp.com/`

### Routes
`/`

`/movie/:movieId`

`/trending`

`/popular`

`/search/:searchVal`

`/actor/:actorId`


