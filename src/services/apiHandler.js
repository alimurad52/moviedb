import axios from'axios';

//global function to be used for making axios rest api call
export function getRequest(url, next) {
    axios.get(url)
        .then((response) => {
            next(prepareResponse(response));
        })
        .catch((err) => {
            next(prepareResponse(err));
        });
}
//to prepare the response to keep it consistent and as required
function prepareResponse(response) {
    if(response.status === 200) {
        return {
            success: true,
            data: response.data,
            status: response.status
        }
    } else {
        return {
            error: true,
            success: false,
            message: 'Api failed to fetch data'
        }
    }
}
