import React from 'react';
import { Router, Route, withRouter, Switch } from "react-router-dom";
import history from './history';
import Home from "../layout/home/Home";
import Navbar from "../components/navbar/Navbar";
import Movie from "../layout/movie/Movie";
import PopularLayout from "../layout/popular/PopularLayout";
import TrendingLayout from "../layout/trending/TrendingLayout";
import Search from "../layout/search/Search";
import Actor from "../layout/actor/Actor";
import NotFound from "../layout/notFound/NotFound";

window.routerHistory = history;
//to navigate the routher programatically adding the history to window
window.routerHistory = history;

const Routes = () => {
    return (
        <Router history={history}>
            <Route component={withRouter(Navbar)} />
            <Switch>
                <Route exact path={`/`} component={withRouter(Home)} />
                <Route exact path={`/movie/:id`} component={withRouter(Movie)}/>
                <Route exact path={`/popular`} component={withRouter(PopularLayout)} />
                <Route exact path={`/trending`} component={withRouter(TrendingLayout)} />
                <Route exact path={`/search/:searchVal`} component={withRouter(Search)} />
                <Route exact path={`/actor/:actorId`} component={withRouter(Actor)} />
                <Route path='*' component={withRouter(NotFound)} />
            </Switch>
        </Router>
    )
};

export default Routes;
