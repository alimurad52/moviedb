import React from 'react';
import {getTrending} from "../../store/actions/homeAction";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Pagination from "react-js-pagination";

class TrendingLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            trending: this.props.trending,
            activePage: 1,
            totalItemCount: 0
        };
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        // first call to get the latest trending data
        this.props.getTrending(this.state.activePage);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //if props change to update the state
        if(this.props.trending !== prevProps.trending) {
            this.setState({trending: this.props.trending, totalItemCount: this.props.trending.total_results});
        }
    }
    //pagination listener
    handlePageChange(page) {
        this.setState({activePage: page}, () => this.props.getTrending(page));
    }

    render() {
        return (
            <section className="container TrendingLayoutContainer">
                <h3>Trending</h3>
                <div className="row mt-2">
                    {
                        this.state.trending.results ? this.state.trending.results.map((item, i) => {
                            return <Link to={`/movie/${item.id}`} key={i}><div className="card px-3" style={{width: '11rem'}}>
                                <img src={item.poster_path ? `https://image.tmdb.org/t/p/w500${item.poster_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.original_title} />
                                <div className="card-body">
                                    <p className="card-title">{item.original_title || item.name}</p>
                                </div>
                            </div></Link>
                        }) : ''
                    }
                </div>
                <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={10}
                    totalItemsCount={this.state.totalItemCount}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                    activeLinkClass={'currentPage'}
                    nextPageText={'Next'}
                    prevPageText={'Prev'}
                    itemClassFirst={'d-none'}
                    itemClassLast={'d-none'}
                    itemClassPrev={'prev'}
                    itemClassNext={'next'}
                />
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    let trending = state.home.trending;
    return {
        trending
    }
};
const mapDispatchToProps = dispatch => ({
    getTrending: (page) => dispatch(getTrending(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(TrendingLayout)
