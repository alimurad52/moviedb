import React from 'react';
import {getPopular} from "../../store/actions/homeAction";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Pagination from "react-js-pagination";

class PopularLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            popular: this.props.popular,
            activePage: 1,
            totalItemCount: 0
        }
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        // first call to get the latest popular data
        this.props.getPopular(this.state.activePage);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //if props change to update the state
        if(this.props.popular !== prevProps.popular) {
            this.setState({popular: this.props.popular, totalItemCount: this.props.popular.total_results});
        }
    }
    //pagination listener
    handlePageChange(page) {
        this.setState({activePage: page}, () => this.props.getPopular(page));
    }

    render() {
        return (
            <section className="container PopularLayoutContainer">
                <h3>Popular</h3>
                <div className="row">
                    {
                        this.state.popular.results ? this.state.popular.results.map((item, i) => {
                            return <Link to={`/movie/${item.id}`} key={i}><div className="card px-3" style={{width: '11rem'}}>
                                <img src={item.poster_path ? `https://image.tmdb.org/t/p/w500${item.poster_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.original_title || item.name} />
                                <div className="card-body  p-0">
                                    <p className="card-title text-center p-1">{item.original_title || item.name}</p>
                                </div>
                            </div></Link>
                        }) : ''
                    }
                </div>
                <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={10}
                    totalItemsCount={this.state.totalItemCount}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                    activeLinkClass={'currentPage'}
                    nextPageText={'Next'}
                    prevPageText={'Prev'}
                    itemClassFirst={'d-none'}
                    itemClassLast={'d-none'}
                    itemClassPrev={'prev'}
                    itemClassNext={'next'}
                />
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    let popular = state.home.popular;
    return {
        popular
    }
};
const mapDispatchToProps = dispatch => ({
    getPopular: (page) => dispatch(getPopular(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(PopularLayout)
