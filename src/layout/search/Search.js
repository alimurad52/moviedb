import React from "react";
import {getSearch} from "../../store/actions/searchAction";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Pagination from "react-js-pagination";

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchResult: this.props.searchResult || [],
            totalItemCount: 0,
            activePage: 1
        };
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        //retrieve the params from the route (url)
        this.props.getSearch(this.props.match.params.searchVal, this.state.activePage);
    }
    //onchange listener for pagination module
    handlePageChange(page) {
        this.setState({activePage: page}, ()=> this.props.getSearch(this.props.match.params.searchVal, this.state.activePage))
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //on change result to update states
        if(this.props.searchResult !== prevProps.searchResult) {
            this.setState({
                searchResult: this.props.searchResult,
                totalItemCount: this.props.searchResult.total_results
            });
        }
        //on change search value in the url to update the reusult
        if(this.props.location !== prevProps.location) {
            this.props.getSearch(this.props.match.params.searchVal, this.state.activePage);
        }
    }

    render() {
        return (
            <section className="container SearchLayoutContainer">
                <h3 className={this.state.searchResult.results ? this.state.searchResult.results.length > 0 ? '' : 'd-none' : 'd-none'}>Search result for <strong>{this.props.match.params.searchVal}</strong></h3>
                <h3 className={this.state.searchResult.results ? this.state.searchResult.results.length > 0 ? 'd-none' : '':''}>No search result for <strong>{this.props.match.params.searchVal}</strong></h3>
                <div className="row mt-2">
                    {
                        this.state.searchResult.results ? this.state.searchResult.results.length > 0 ? this.state.searchResult.results.map((item, i) => {
                            return <Link to={`/movie/${item.id}`} key={i}><div className="card px-3" style={{width: '11rem'}}>
                                <img src={item.poster_path ? `https://image.tmdb.org/t/p/w500${item.poster_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.original_title || item.name} />
                                <div className="card-body  p-0">
                                    <p className="card-title text-center p-1">{item.original_title || item.name}</p>
                                </div>
                            </div></Link>
                        }) : '' : ''
                    }
                </div>
                <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={10}
                    totalItemsCount={this.state.totalItemCount}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                    activeLinkClass={'currentPage'}
                    nextPageText={'Next'}
                    prevPageText={'Prev'}
                    itemClassFirst={'d-none'}
                    itemClassLast={'d-none'}
                    itemClassPrev={'prev'}
                    itemClassNext={'next'}
                />
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    let searchResult = state.search.searchResult;
    return {
        searchResult
    };
};
const mapDispatchToProps = dispatch => ({
    getSearch: (searchQuery, activePage) => dispatch(getSearch(searchQuery, activePage))
});

export default connect(mapStateToProps, mapDispatchToProps)(Search)
