import React from 'react';
import {connect} from "react-redux";
import {getRequest} from "../../services/apiHandler";
import Moment from 'react-moment';
import {Link} from "react-router-dom";

class Movie extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            languages: [],
            adult: false,
            image: '',
            productionCompanies: [],
            productionCountries: [],
            revenue: [],
            tagline: '',
            title: '',
            runtime: 0,
            overview: '',
            releaseDate: '',
            budget: '',
            genres: []
        };
        this.getMovie = this.getMovie.bind(this);
    }

    async componentDidMount() {
        //asyncronously get the movies
        await this.getMovie();
    }

    getMovie() {
        //return promise for an async call
        return new Promise((resolve, reject) => {
            getRequest(`https://api.themoviedb.org/3/movie/${this.props.match.params.id}?api_key=0ad911754cf6780371d969823a0af38f&language=en-US&append_to_response=credits`, (ret) => {
                //upon success set all states
                if(ret.success) {
                    this.setState({
                        languages: ret.data.spoken_languages,
                        adult: ret.data.adult,
                        image: ret.data.poster_path,
                        productionCompanies: ret.data.production_companies,
                        productionCountries: ret.data.production_countries,
                        revenue: ret.data.revenue,
                        tagline: ret.data.tagline,
                        title: ret.data.title,
                        runtime: ret.data.runtime,
                        overview: ret.data.overview,
                        releaseDate: ret.data.release_date,
                        cast: ret.data.credits.cast,
                        budget: ret.data.budget,
                        genres: ret.data.genres,
                        originalTitle: ret.data.original_title
                    }, () => { return resolve() });
                }
            })
        })

    }

    render() {
        return (
            <div className="container MovieContainer container">
                <h3>{this.state.title}</h3>
                    <div className="row">
                        <div className="col-lg-3 col-md-2 col-sm-12">
                            <img src={this.state.image ? `https://image.tmdb.org/t/p/w500${this.state.image}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top rounded-0" alt={this.state.title || this.state.originalTitle} />
                        </div>
                        <div className="col-lg-9 col-md-8 col-sm-12">
                            <table className="table">
                                <tbody>
                                <tr>
                                    <td>Title:</td>
                                    <td>{this.state.title}</td>
                                </tr>
                                <tr className={this.state.runtime? '':'d-none'}>
                                    <td>Runtime:</td>
                                    <td>{this.state.runtime} mins</td>
                                </tr>
                                <tr className={this.state.releaseDate? '':'d-none'}>
                                    <td>Release Year:</td>
                                    <td><Moment format={'YYYY'}>{this.state.releaseDate}</Moment></td>
                                </tr>
                                <tr className={this.state.genres.length > 0 ? '':'d-none'}>
                                    <td>Genres:</td>
                                    <td>{
                                        this.state.genres.map((item, i) => {
                                            return <span className="badge badge-secondary mr-2" key={i}>{item.name}</span>
                                        })
                                    }</td>
                                </tr>
                                <tr className={this.state.budget? '':'d-none'}>
                                    <td>Budget:</td>
                                    <td>${Number(this.state.budget).toLocaleString('en')}</td>
                                </tr>
                                <tr className={this.state.overview ? '':'d-none'}>
                                    <td>Overview:</td>
                                    <td>{this.state.overview}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <h3 className={this.state.cast ? this.state.cast.length > 0 ? '':'d-none':'d-none'}>Cast</h3>
                <div className="row">
                    {
                        this.state.cast ? this.state.cast.map((item, i) => {
                            return <Link key={i} to={`/actor/${item.id}`}><div className="card h-100 px-3" style={{width: '11rem'}} >
                                <img src={item.profile_path ? `https://image.tmdb.org/t/p/w500${item.profile_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.character} />
                                <div className="card-body  p-0">
                                    <p className="card-title text-center p-1"><strong>{item.name}</strong> as <strong>{item.character}</strong></p>
                                </div>
                            </div></Link>
                        }) : ""
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {

    }
};
const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Movie)
