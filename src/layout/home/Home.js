import React from 'react';
import { connect } from "react-redux";
import Popular from "../../components/popular/Popular";
import InTheatre from "../../components/inTheatre/InTheatre";
import Trending from "../../components/trending/Trending";

class Home extends React.Component {
    //main container to contain all the homepage components
    render() {
        return (
            <div>
                <InTheatre />
                <Popular />
                <Trending />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    };
};
const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Home)
