import React from "react";
import {connect} from "react-redux";
import {getActor} from "../../store/actions/actorAction";
import Moment from "react-moment";
import {Link} from "react-router-dom";

class Actor extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            actor: this.props.actor || {}
        }
    }
    //make initial call to get the actor details
    componentDidMount() {
        this.props.getActor(this.props.match.params.actorId)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //on change props update the state
        if(this.props.actor !== prevProps.actor) {
            this.setState({
                actor: this.props.actor
            });
        }
    }

    render() {
        return (
            <div className="container ActorContainer container">
                <h3>{this.state.actor.name}</h3>
                <div className="row">
                    <div className="col-lg-3 col-md-2 col-sm-12">
                        <img src={this.state.actor.profile_path ? `https://image.tmdb.org/t/p/w500${this.state.actor.profile_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top rounded-0" alt={this.state.actor.original_title || this.state.actor.name} />
                    </div>
                    <div className="col-lg-9 col-md-10 col-sm-12">
                        <table className="table">
                            <tbody>
                            <tr className={this.state.actor.birthday ? '':'d-none'}>
                                <td>Birthday:</td>
                                <td><Moment format={'LL'}>{this.state.actor.birthday}</Moment></td>
                            </tr>
                            <tr className={this.state.actor.deathday? '':'d-none'}>
                                <td>Death Day:</td>
                                <td><Moment format={'LL'}>{this.state.actor.deathday}</Moment></td>
                            </tr>
                            <tr className={this.state.actor.place_of_birth? '':'d-none'}>
                                <td>Place of Birth:</td>
                                <td>{this.state.actor.place_of_birth}</td>
                            </tr>
                            <tr className={this.state.actor.biography ? '':'d-none'}>
                                <td>Biography</td>
                                <td>{this.state.actor.biography}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h4 className="py-4">More movies by {this.state.actor.name}</h4>
                <div className="row">

                    {
                        this.state.actor.movie_credits ? this.state.actor.movie_credits.cast.map((item, i) => {
                            return <Link key={i} to={`/movie/${item.id}`}><div className="card h-100 px-3" style={{width: '11rem'}} >
                                <img src={item.poster_path ? `https://image.tmdb.org/t/p/w500${item.poster_path ? item.poster_path : ''}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.title} />
                                <div className="card-body  p-0">
                                    <p className="card-title text-center p-1">{item.title}</p>
                                </div>
                            </div></Link>
                        }):""
                    }
                </div>

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    let actor = state.actor.actor;
    return {
        actor
    };
};
const mapDispatchToProps = dispatch => ({
    getActor: (id) => dispatch(getActor(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Actor)
