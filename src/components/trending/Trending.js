import React from 'react';
import {connect} from "react-redux";
import {getTrending} from "../../store/actions/homeAction";
import {Link} from "react-router-dom";

class Trending extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            trending: this.props.trending
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //if props change to update the state
        if(this.props.trending !== prevProps.trending) {
            this.setState({trending: this.props.trending});
        }
    }

    componentDidMount() {
        // first call to get the latest trending data
        this.props.getTrending(1);
    }

    render() {
        return (
            <section className="container TrendingContainer px-3 py-5">
                <h3>Trending</h3>
                <div className="row mt-2">
                    {
                        this.state.trending.results ? this.state.trending.results.map((item, i) => {
                            return <Link to={`/movie/${item.id}`} key={i}><div className="card h-100 px-3" style={{width: '11rem'}}>
                                <img src={item.poster_path ? `https://image.tmdb.org/t/p/w500${item.poster_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.original_title || item.name} />
                                <div className="card-body  p-0">
                                    <p className="card-title text-center p-1">{item.original_title || item.name}</p>
                                </div>
                            </div></Link>
                        }) : ''
                    }
                </div>
                <Link className="btn float-right see-all-btn bt-main" to={`/trending`}>See All</Link>
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    let trending = state.home.trending;
    return {
        trending
    }
};
const mapDispatchToProps = dispatch => ({
    getTrending: (page) => dispatch(getTrending(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(Trending)
