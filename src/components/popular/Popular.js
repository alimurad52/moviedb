import React from 'react';
import {connect} from "react-redux";
import {getPopular} from "../../store/actions/homeAction";
import {Link} from "react-router-dom";

class Popular extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            popular: this.props.popular
        }
    }

    componentDidMount() {
        //first call to get the popular
        this.props.getPopular(1);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //update state on change of props
        if(this.props.popular !== prevProps.popular) {
            this.setState({popular: this.props.popular});
        }
    }

    render() {
        return (
            <section className="container PopularContainer px-3 py-5">
                <h3>Popular</h3>
                <div className="row mt-2">
                {
                    this.state.popular.results ? this.state.popular.results.map((item, i) => {
                        return <Link to={`/movie/${item.id}`} key={i}><div className="card px-3" style={{width: '11rem'}}>
                                <img src={item.poster_path ? `https://image.tmdb.org/t/p/w500${item.poster_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.title} />
                                    <div className="card-body  p-0">
                                        <p className="card-title text-center p-1">{item.original_title || item.name}</p>
                                    </div>
                        </div></Link>
                    }) : ''
                }
                </div>
                <Link className="btn float-right bt-main see-all-btn" to={`/popular`}>See All</Link>
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    let popular = state.home.popular;
    return {
        popular
    };
};
const mapDispatchToProps = dispatch => ({
    getPopular: (page) => dispatch(getPopular(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(Popular)
