import React from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {getSearch} from "../../store/actions/searchAction";

class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchVal: '',
            activeNavbar: 'home'
        };
        this.onChangeSearch = this.onChangeSearch.bind(this);
        this.onClickSubmit = this.onClickSubmit.bind(this);
    }
    //on change input value set to state
    onChangeSearch(e) {
        this.setState({searchVal: e.target.value});
    }
    //on form submit
    onClickSubmit(e) {
        e.preventDefault();
        //check if the search string has value
        if(this.state.searchVal) {
            //programmatically route to search page
            window.routerHistory.push(`/search/${this.state.searchVal}`)
        }
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light">
                <div className="container">
                    <Link className="navbrand" to={'/'}>Movie DB</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon mr-auto"></span>
                    </button>

                    <div className="collapse navbar-collapse float-right" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto">
                            <li className={"nav-item mr-2 " + (this.props.location.pathname === '/' ? 'active' : '')}>
                                <Link to={'/'} className="nav-link">Home <span className="sr-only">(current)</span></Link>
                            </li>
                            <li className={"nav-item mr-2 " + (this.props.location.pathname.includes('trending') ? 'active' : '')}>
                                <Link to={'/trending'} className="nav-link">Trending</Link>
                            </li>
                            <li className={"nav-item mr-2 "+ (this.props.location.pathname.includes('popular') ? 'active' : '')}>
                                <Link to={'/popular'} className="nav-link">Popular</Link>
                            </li>
                        </ul>
                        <form className="form-inline my-2 my-lg-0" onSubmit={this.onClickSubmit}>
                            <input className="form-control mr-sm-2" value={this.state.searchVal} placeholder="Search" aria-label="Search" onChange={(e) => this.onChangeSearch(e)} />
                            {/*<button  onClick={(e) => this.onChangeSearch(e.target.val)}>Search</button>*/}
                            <input type="submit" className='btn bt-inverse my-2 my-sm-0' value="Search"  />
                        </form>
                    </div>
                </div>
            </nav>
        )
    }
}

const mapStateToProps = (state) => {
    return {
    };
};
const mapDispatchToProps = dispatch => ({
    getSearch: (searchQuery) => dispatch(getSearch(searchQuery))
});

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
