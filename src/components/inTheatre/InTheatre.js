import React from 'react';
import {connect} from "react-redux";
import {getInTheatre} from "../../store/actions/homeAction";
import 'react-multi-carousel/lib/styles.css';
import {Link} from "react-router-dom";
import ItemsCarousel from 'react-items-carousel';

class InTheatre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inTheatre: this.props.inTheatre,
            activeItem: 1
        };
        this.changeActiveItem = this.changeActiveItem.bind(this);
        this.tick = this.tick.bind(this);
    }

    componentDidMount() {
        //initial call to the get the movies
        this.props.getInTheatre();
        this.interval = setInterval(this.tick, 1500);
    }

    componentWillUnmount() {
        //to disable interval upon exit from component
        clearInterval(this.interval);
    }
    //to atuoplay the carousel setting the activeItem
    tick() {
        this.setState(prevState => ({
            activeItem: (prevState.activeItem + 1) % (this.state.inTheatre.results ? this.state.inTheatre.results.length  - 3 + 1: ''),
        }));
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //upon update of props, update state
        if(this.props.inTheatre !== prevProps.inTheatre) {
            this.setState({
                inTheatre: this.props.inTheatre
            })
        }
    }
    //onchange active item for carousel
    changeActiveItem(index) {
        this.setState({ activeItem: index });
    }

    render() {
        return (
            <section className="CarouselContainer container px-3 pt-5">
                <h3>Now Playing</h3>
                <ItemsCarousel
                    // Carousel configurations
                    infiniteLoop={true}
                    numberOfCards={5}
                    gutter={10}
                    firstAndLastGutter={true}
                    freeScrolling={false}
                    transitionDuration={1500}
                    autoPlay={true}
                    autoPlaySpeed={1500}
                    showSlither={true}

                    // Active item configurations
                    requestToChangeActive={this.changeActiveItem}
                    activeItemIndex={this.state.activeItem}
                    activePosition={'center'}

                    //for arrows
                    chevronWidth={30}
                    rightChevron={<i className="fas fa-chevron-right fa-2x"></i>}
                    leftChevron={<i className="fas fa-chevron-left fa-2x"></i>}
                    outsideChevron={false}
                >
                    {
                        this.state.inTheatre.results ? this.state.inTheatre.results.map((item, i) => {
                            return <Link to={`/movie/${item.id}`} key={i}><div className="card px-3" style={{width: '11rem'}}>
                                <img src={item.poster_path ? `https://image.tmdb.org/t/p/w500${item.poster_path}` : 'https://via.placeholder.com/550?text=No+Image'} className="card-img-top" alt={item.original_title || item.name} />
                                <div className="card-body  p-0">
                                    <p className="card-title text-center p-1">{item.original_title || item.name}</p>
                                </div>
                            </div></Link>
                        }) : ''
                    }
                </ItemsCarousel>
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    let inTheatre = state.home.inTheatre;
    return {
        inTheatre
    };
};
const mapDispatchToProps = dispatch => ({
    getInTheatre: () => dispatch(getInTheatre())
});

export default connect(mapStateToProps, mapDispatchToProps)(InTheatre)
