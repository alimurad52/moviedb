import {GET_IN_THEATRE, GET_POPULAR, GET_TRENDING} from "../actions/actionTypes";

const INITIAL_STATE = {
    inTheatre: [],
    popular: [],
    trending: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_IN_THEATRE:
            return Object.assign({}, state, {
                inTheatre: action.payload
            });
        case GET_POPULAR:
            return Object.assign({}, state, {
                popular: action.payload
            });
        case GET_TRENDING:
            return Object.assign({}, state, {
                trending: action.payload
            });
        default:
            return state;
    }
};
