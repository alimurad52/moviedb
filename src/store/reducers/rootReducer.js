import { combineReducers } from "redux";
import hReducer from './hReducer';
import sReducer from './sReducer';
import aReducer from './aReducer';


const rootReducer = combineReducers({
    home: hReducer,
    search: sReducer,
    actor: aReducer
});
export default rootReducer;
