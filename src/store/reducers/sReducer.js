import {GET_SEARCH} from "../actions/actionTypes";

const INITIAL_STATE = {
    searchResult: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_SEARCH:
            return Object.assign({}, state, {
                searchResult: action.payload
            });
        default:
            return state;
    }
};
