import {GET_ACTOR} from "../actions/actionTypes";

const INITIAL_STATE = {
    actor: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_ACTOR:
            return Object.assign({}, state, {
                actor: action.payload
            });
        default:
            return state;
    }
};
