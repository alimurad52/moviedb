//actions type to be used for redux call

//home
export const GET_IN_THEATRE = 'GET_IN_THEATRE';
export const GET_POPULAR = 'GET_POPULAR';
export const GET_TRENDING = 'GET_TRENDING';

//search
export const GET_SEARCH = 'GET_SEARCH';

//actor
export const GET_ACTOR = 'GET_ACTOR';


