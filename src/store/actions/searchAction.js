import {GET_SEARCH} from "./actionTypes";
import {getRequest} from "../../services/apiHandler";

export const getSearch = (searchQuery, page) => {
    return (dispatch) => {
        getRequest(`https://api.themoviedb.org/3/search/movie?api_key=0ad911754cf6780371d969823a0af38f&language=en-US&page=${page}&include_adult=false&query=${searchQuery}`, (ret) => {
            if(ret.success) {
                dispatch(setSearch(ret.data));
            }
        });
    }
};

function setSearch(res) {
    return {
        type: GET_SEARCH,
        payload: res
    }
}
