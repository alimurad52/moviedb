import {getRequest} from "../../services/apiHandler";
import {GET_ACTOR} from "./actionTypes";

export const getActor = (id) => {
    return (dispatch) => {
        getRequest(`https://api.themoviedb.org/3/person/${id}?api_key=0ad911754cf6780371d969823a0af38f&language=en-US&append_to_response=movie_credits`, (ret) => {
            if(ret.success) {
                dispatch(setActor(ret.data))
            }
        })
    }
};

function setActor(res) {
    return {
        type: GET_ACTOR,
        payload: res
    }
}
