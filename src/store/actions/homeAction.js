import {getRequest} from "../../services/apiHandler";
import {GET_IN_THEATRE, GET_POPULAR, GET_TRENDING} from "./actionTypes";

export const getInTheatre = () => {
    return (dispatch) => {
        getRequest(`https://api.themoviedb.org/3/movie/now_playing?api_key=0ad911754cf6780371d969823a0af38f&language=en-US&page=1&include_adult=false`, (ret) => {
            if(ret.success) {
                dispatch(setMovies(ret.data))
            }
        })
    }
};

export const getPopular = (page) => {
    return (dispatch) => {
        getRequest(`https://api.themoviedb.org/3/discover/movie?api_key=0ad911754cf6780371d969823a0af38f&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}`, (ret) => {
            if(ret.success) {
                dispatch(setPopular(ret.data));
            }
        })
    }
};

export const getTrending = (page) => {
    return (dispatch) => {
        getRequest(`https://api.themoviedb.org/3/trending/all/day?api_key=0ad911754cf6780371d969823a0af38f&page=${page}&include_adult=false`, (ret) => {
            if(ret.success) {
                dispatch(setTrending(ret.data));
            }
        })
    }
}

function setMovies(res) {
    return {
        type: GET_IN_THEATRE,
        payload: res
    }
}

function setPopular(res) {
    return {
        type: GET_POPULAR,
        payload: res
    }
}

function setTrending(res) {
    return {
        type: GET_TRENDING,
        payload: res
    }
}
